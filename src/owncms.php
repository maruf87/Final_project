<?php
namespace owncms;
if(!isset($_SESSION)){session_start();}

class owncms{
	public $id = '';
	public $uniqid = '';
	public $user_id = '';
        public $users_id = '';
	public $conn = '';
	public $date = '';
	public $username = '';
	public $password = '';
	public $email = '';
	public $firstname = '';
	public $lastname = '';
	public $personalphone = '';
	public $homephone = '';
	public $officephone = '';
	public $currentaddress = '';
	public $permanentaddress = '';
	public $image = '';
	public $confirm = '';
	public $disable = '';
	public $active = '';
	public $trash = '';
	public $restore = '';
	public $menu = '';
	public $category = '';	
	public $title = '';
	public $subtitle = '';
	public $html_details = '';
	public $details = '';
	public $cat_id = '';



	public function __construct(){
		$this->conn = mysqli_connect("localhost","root","","owncms");
		$this->date = date('Y-m-d h:i:s');
		return $this;
	}
	public function prepare($data){
		if (!empty($data['id'])) {
			$this->id = $data['id'];
		}
		if (!empty($data['user_id'])) {
			$this->user_id = $data['user_id'];
		}
		if (!empty($data['uniqid'])) {
			$this->uniqid = $data['uniqid'];
		}
		if (!empty($data['username']) && !empty($data['password']) && !empty($data['email'])) {
			$this->username = $data['username'];
			$this->password = $data['password'];
			$this->email = $data['email'];
		}
		if (!empty($data['username']) && !empty($data['password'])) {
			$this->username = $data['username'];
			$this->password = $data['password'];
		}
		if (!empty($data['uniq_id'])) {
			$this->uniqid = $data['uniq_id'];
			$this->user_id = $data['user_id'];
			$this->firstname = $data['firstname'];
			$this->lastname = $data['lastname'];
			$this->email = $data['email'];
			$this->personalphone = $data['personalphone'];
			$this->homephone = $data['homephone'];
			$this->officephone = $data['officephone'];
			$this->currentaddress = $data['currentaddress'];
			$this->permanentaddress = $data['permanentaddress'];
			$this->password = $data['password'];
			$this->image = $data['image'];
		}
		if (isset($data['confirm'])) {
			$this->confirm = $data['confirm'];
		}elseif (isset($data['disable'])) {
			$this->disable = $data['disable'];
		}elseif (isset($data['active'])) {
			$this->active = $data['active'];
		}elseif (isset($data['trash'])) {
			$this->trash = $data['trash'];
		}elseif (isset($data['restore'])) {
			$this->restore = $data['restore'];
		}
		if (!empty($data['menu'])) {
			$this->menu = $data['menu'];
		}
		if (!empty($data['category'])) {
			$this->category = $data['category'];
		}
		if (!empty($data['title'])) {
			$this->title = $data['title'];
			$this->subtitle = $data['subtitle'];
			$this->html_details = $data['html_details'];
			$this->details = $data['details'];
			$this->user_id = $data['user_id'];
			$this->cat_id = $data['category'];
		}
		return $this;
	}
	public function signup(){
		if (!empty($this->username) && !empty($this->password) && !empty($this->email)) {
			$unique_id = uniqid();
			$query = "INSERT INTO users (unique_id,username,password,email,created_at) VALUES ('".$unique_id."','".$this->username."','".$this->password."','".$this->email."','".$this->date."')";
			if (mysqli_query($this->conn,$query)) {
				$last_id = $this->conn->insert_id;
				$query = "INSERT INTO profiles (user_id,created_at) VALUES ('".$last_id."','".$this->date."')";
				if (mysqli_query($this->conn,$query)) {
					$_SESSION['signup'] = "Registration successful.<br> Please wait for admin approbation.";
				}
				else{
					$_SESSION['signup'] = "Error!";
				}
			}else{
				$_SESSION['signup'] = "This username or email has  already been Exist!";
			}
		}else{
			$_SESSION['signup'] = "Invailed username or password!";
		}
		header('location:../signup.php');
	}
	public function login(){
		if (!empty($this->username) && !empty($this->password)) {
			$query = "SELECT * FROM users WHERE username = '".$this->username."' AND password = '".$this->password."' AND is_active = 1";
			$result = mysqli_query($this->conn,$query);
			$row = mysqli_fetch_assoc($result);
			if ($row) {
				$_SESSION['username'] = $row['username'];
				$_SESSION['uniqid'] = $row['unique_id'];
				$_SESSION['user_id'] = $row['id'];
				$_SESSION['is_admin'] = $row['is_admin'];
				header('location:../dashboard/index.php');
			}else{
				$_SESSION['login'] = "Login failed!";
				header('location:../login.php');
			}
		}else{
			$_SESSION['login'] = "Please signup.";
			header('location:../login.php');
		}
	}
	public function profiles(){
		$query = "SELECT * FROM users RIGHT JOIN profiles ON profiles.user_id = users.id WHERE is_active != 3 OR is_active IS NULL";
		$result = mysqli_query($this->conn,$query);
		while ($row = mysqli_fetch_assoc($result)){
			$array[] = $row;
		}
		if (!empty($array)) { return $array; }		
	}
	public function profile(){
		if(!empty($this->uniqid)){
			$query = "SELECT * FROM users RIGHT JOIN profiles ON profiles.user_id = users.id WHERE unique_id = '".$this->uniqid."'";
			$result = mysqli_query($this->conn,$query);
			$row = mysqli_fetch_assoc($result);
			return $array = $row;
		}
	}
	public function update_profile(){
		if(isset($this->image) && !empty($this->image)){
			$sql = "SELECT profile_pic FROM users RIGHT JOIN profiles ON profiles.user_id = users.id WHERE unique_id = '".$this->uniqid."'";
			$res = mysqli_query($this->conn,$sql);
			$unlink = mysqli_fetch_assoc($res);
			unlink("../images/".$unlink['profile_pic']);
			$query = "UPDATE users,profiles SET users.email = '".$this->email."',users.password = '".$this->password."',users.modified_at = '".$this->date."',profiles.first_name = '".$this->firstname."',profiles.last_name = '".$this->lastname."',profiles.personal_phone = '".$this->personalphone."',profiles.home_phone = '".$this->homephone."',profiles.office_phone = '".$this->officephone."',profiles.current_address = '".$this->currentaddress."',profiles.permanent_address = '".$this->permanentaddress."',profiles.profile_pic = '".$this->image."',profiles.modified_at = '".$this->date."',profiles.modified_by = '".$this->user_id."' WHERE users.id = profiles.user_id AND users.unique_id = '".$this->uniqid."'";
		}else{
			$query = "UPDATE users,profiles SET users.email = '".$this->email."',users.password = '".$this->password."',users.modified_at = '".$this->date."',profiles.first_name = '".$this->firstname."',profiles.last_name = '".$this->lastname."',profiles.personal_phone = '".$this->personalphone."',profiles.home_phone = '".$this->homephone."',profiles.office_phone = '".$this->officephone."',profiles.current_address = '".$this->currentaddress."',profiles.permanent_address = '".$this->permanentaddress."',profiles.modified_at = '".$this->date."',profiles.modified_by = '".$this->user_id."' WHERE users.id = profiles.user_id AND users.unique_id = '".$this->uniqid."'";
		}
		if (mysqli_query($this->conn,$query)) {
			$_SESSION['message'] = "Update successful";
		}else{
			$_SESSION['message'] = "Update failed";
		}
		header('location:../dashboard/profile.php');
	}
	public function is_action(){
		if (!empty($this->confirm)) {
			$query = "UPDATE users SET is_active = 1 WHERE unique_id = '".$this->confirm."'";
			mysqli_query($this->conn,$query);
			$_SESSION['message'] = "disabled";
			header('location:../dashboard/allusers.php');
		}elseif (!empty($this->disable)) {
			$query = "UPDATE users SET is_active = 2 WHERE unique_id = '".$this->disable."'";
			mysqli_query($this->conn,$query);
			$_SESSION['message'] = "disabled";
			header('location:../dashboard/allusers.php');
		}elseif (!empty($this->active)) {
			$query = "UPDATE users SET is_active = 1 WHERE unique_id = '".$this->active."'";
			mysqli_query($this->conn,$query);
			$_SESSION['message'] = "actived";
			header('location:../dashboard/allusers.php');
		}elseif (!empty($this->trash)) {
			$query = "UPDATE users SET is_active = 3 WHERE unique_id = '".$this->trash."'";
			mysqli_query($this->conn,$query);
			$_SESSION['message'] = "trashed";
			header('location:../dashboard/allusers.php');
                
		}elseif (!empty($this->restore)) {
			$query = "UPDATE users SET is_active = 1 WHERE unique_id = '".$this->restore."'";
			mysqli_query($this->conn,$query);
			$_SESSION['message'] = "restore";
			header('location:../dashboard/allusers.php?users=trash');
		}else{
			$_SESSION['message'] = "failed";
		}
	}
	public function trash_users(){
		$query = "SELECT * FROM users RIGHT JOIN profiles ON profiles.user_id = users.id WHERE is_active = 3";
		$result = mysqli_query($this->conn,$query);
		while ($row = mysqli_fetch_assoc($result)){
			$array[] = $row;
		}
		if (!empty($array)) { return $array; }	
	}
	public function newmenu(){
		if (!empty($this->menu)) {
			$query = "INSERT INTO menus (title,created_at) VALUES ('".$this->menu."','".$this->date."')";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Menu Added successfully .";
			}else{
				$_SESSION['message'] = "Failed";
			}
		}else{
			$_SESSION['message'] = "Menu field empty!";
		}
		header('location:../dashboard/addmenu.php');
	}
	public function menus(){
		$query = "SELECT * FROM menus";
		$result = mysqli_query($this->conn,$query);
		while ($row = mysqli_fetch_assoc($result)){
			$array[] = $row;
		}
		if (!empty($array)) {
			return $array;
		}	
	}
	public function menuID(){
		if (!empty($this->id)) {
			$query = "SELECT * FROM menus WHERE id = $this->id";
			$result = mysqli_query($this->conn,$query);
			$row = mysqli_fetch_assoc($result);
			return $row;
		}
	}
	public function updatemenu(){
		if (!empty($this->menu)) {
			$query = "UPDATE menus SET title = '".$this->menu."',modified_at = '".$this->date."' WHERE id = $this->id";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Manu Update successful";
			}else{
				$_SESSION['message'] = "Manu Update failed";
			}
			header('location:../dashboard/editmenu.php?id='.$this->id);
		}
	}
	public function deletemenu(){
		if (!empty($this->id)) {
			$query = "DELETE FROM menus WHERE id = $this->id";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Delete successful";
			}else{
				$_SESSION['message'] = "Delete failed";
			}
			header('location:../dashboard/allposts.php');
		}
	}
	public function newcategory(){
		if (!empty($this->category)) {
			$query = "INSERT INTO categories (title,created_at) VALUES ('".$this->category."','".$this->date."')";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Category added successfully";
			}else{
				$_SESSION['message'] = "Category added failed";
			}
		}else{
			$_SESSION['message'] = "Please Add category ";
		}
		header('location:../dashboard/addcategory.php');
	}
	public function categories(){
		$query = "SELECT * FROM categories";
		$result = mysqli_query($this->conn,$query);
		while ($row = mysqli_fetch_assoc($result)){
			$array[] = $row;
		}
		if (!empty($array)) {
			return $array;
		}	
	}
	public function categoryID(){
		if (!empty($this->id)) {
			$query = "SELECT * FROM categories WHERE id = $this->id";
			$result = mysqli_query($this->conn,$query);
			$row = mysqli_fetch_assoc($result);
			return $row;
		}
	}
	public function updatecategory(){
		if (!empty($this->category)) {
			$query = "UPDATE categories SET title = '".$this->category."',updated_at = '".$this->date."' WHERE id = $this->id";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Catagory Update successful";
			}else{
				$_SESSION['message'] = "Catagory update failed";
			}
			header('location:../dashboard/editcategory.php?id='.$this->id);
		}
	}
	public function deletecategory(){
		if (!empty($this->id)) {
			$query = "DELETE FROM categories WHERE id = $this->id";
			if (mysqli_query($this->conn,$query)) {
				$_SESSION['message'] = "Catagory Deleted successfully";
			}else{
				$_SESSION['message'] = "Delete failed";
			}
			header('location:../dashboard/addcategory.php');
		}
	}

	public function newpost(){
		if (!empty($this->title)) {
			$queryone = "INSERT INTO articles (users_id,title,sub_title,details,html_details,created_at) VALUES ('".$this->user_id."','".$this->title."','".$this->subtitle."','".$this->details."','".$this->html_details."','".$this->date."')";
			if(mysqli_query($this->conn,$queryone)){
				$article_id = $this->conn->insert_id;
				$querytwo = "INSERT INTO articles_categories_mapping (article_id,category_id,created_at) VALUES ('".$article_id."','".$this->cat_id."','".$this->date."')";
				mysqli_query($this->conn,$querytwo);			
				$_SESSION['message'] = "New post Added sussessfully";
			}else{
				$_SESSION['message'] = "failed";
			}
		}else{
			$_SESSION['message'] = "Please Add title.";
		}
		header('location:../dashboard/newpost.php');
	}
	public function articles(){
		$query = "SELECT * FROM articles LEFT JOIN users ON articles.users_id = users.id/* WHERE users_id = '".$this->user_id."'*/";
		$result = mysqli_query($this->conn,$query);
		while ($row = mysqli_fetch_assoc($result)){
			$array[] = $row;
		}
		if (!empty($array)) { return $array; }	
	}

// =========----------------------------------------------------===============>
        
        
        
//        public function deletearticle(){
//		if (!empty($this->id)) {
//			$query = "DELETE FROM `articles` WHERE users_id=$this->user_id";
//			if (mysqli_query($this->conn,$query)) {
//				$_SESSION['message'] = "Articles Deleted successfully";
//			}else{
//				$_SESSION['message'] = "Delete failed";
//			}
//			header('location:../dashboard/allposts.php');
//		}
//	}
//        
//        public function updatearticle(){
//		if (!empty($this->menu)) {
//			$query = "UPDATE articles SET title = '".$this->title."',modified_at = '".$this->date."' WHERE id = $this->id";
//			if (mysqli_query($this->conn,$query)) {
//				$_SESSION['message'] = "Article Update successful";
//			}else{
//				$_SESSION['message'] = "Article Update failed";
//			}
//			header('location:../dashboard/editpost.php');
//		}
//	}
//        


















































}
?>