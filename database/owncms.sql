-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2007 at 07:33 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `owncms`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
`id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `html_summary` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `html_details` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `users_id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`, `created_at`, `modified_at`, `deleted_at`) VALUES
(19, 45, 'Deadline for biometric re-registration of SIM cards extended by a month', ' Staff Correspondent,  bdnews24.com', '', '', '&nbsp;Deadline for biometric re-registration of SIM cards extend by a monthState Minister for Post and Telecommunications Tarana Halim announced the extension during a press conference in the capital on Saturday, just a few hours before the expi', '<p>&nbsp;</p><h2><strong>Deadline for biometric re-registration of SIM cards extended by a month</strong></h2><p><img alt="" src="/109647/final%20projectttttttttt/CodeHunter-final_project%20update/dashboard/kcfinder/upload/images/03_Biometric%2BSI', '2016-05-01 01:49:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 21, 'Bangladesh observes historic May Day with call for workers-owners unity', ' News Desk,  bdnews24.com', '', '', 'Bangladesh observes historic May Day with call for workers-owners unity\r\n\r\nOn May 1, 1886, ten workers were killed when police opened fire on a demonstration in the US city of Chicago near Hay Market demanding an eight-hour working day instead 12 hours.\r\n', '<h1><strong>Bangladesh observes historic May Day with call for workers-owners unity</strong></h1>\r\n\r\n<p>On May 1, 1886, ten workers were killed when police opened fire on a demonstration in the US city of Chicago near Hay Market demanding an eight-hour wo', '2016-05-01 02:15:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 45, 'Fire at Karwan Bazar tamed, burns down 200 shops ', ' Staff Correspondent,  bdnews24.com', '', '', 'Fire at Karwan Bazar tamed, burns down 200 shops&nbsp;\r\n\r\nMost of these shops in the two-storey tin-shed market behind Janata Tower were depots of spices.\r\n&nbsp;\r\nThe fire started around 7:45pm on Sunday, soon after a spell of rain and Nor&rsquo;wester, ', '<h1>Fire at Karwan Bazar tamed, burns down 200 shops&nbsp;</h1>\r\n\r\n<p>Most of these shops in the two-storey tin-shed market behind Janata Tower were depots of spices.<br />\r\n&nbsp;<br />\r\nThe fire started around 7:45pm on Sunday, soon after a spell of rai', '2016-05-01 08:01:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 21, 'Bangladesh steps up public diplomacy with ‘new’ Myanmar', '  Nurul Islam Hasib from Yangon, Myanmar  bdnews24.com', '', '', 'The relations with the new democratic Myanmar are of strategic importance, as they are the gateway to China and ASEAN economies, besides spelling a potential supply of natural gas.In its latest move, the Bangladesh embassy on Saturday showcased the cu', 'The relations with the new democratic Myanmar are of strategic importance, as they are the gateway to China and ASEAN economies, besides spelling a potential supply of natural gas.\r\n\r\nIn its latest move, the Bangladesh embassy on Saturday showcased the cu', '2007-12-31 07:08:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 21, 'Hearty rain showers joy on Dhaka in heat respite', '  Staff Correspondent,  bdnews24.com', '', '', 'Scorched residents in Dhaka and several other districts enjoyed a brief interlude on Sunday in a welcome evening spell of rain and Nor&rsquo;wester.\r\n\r\nThe heavy shower with blustery wind cooled the weather as it swept over the capital.\r\n\r\nThe rain has oc', '<p>Scorched residents in Dhaka and several other districts enjoyed a brief interlude on Sunday in a welcome evening spell of rain and Nor&rsquo;wester.</p>\r\n\r\n<p>The heavy shower with blustery wind cooled the weather as it swept over the capital.</p>\r\n\r\n<', '2007-12-31 07:29:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `articles_categories_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_categories_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_categories_mapping`
--

INSERT INTO `articles_categories_mapping` (`id`, `article_id`, `category_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 19, 2, '2016-05-01 01:49:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 20, 2, '2016-05-01 02:15:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 21, 9, '2016-05-01 08:01:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 22, 9, '2007-12-31 07:08:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 23, 8, '2007-12-31 07:29:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `articles_images_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_images_mapping` (
`id` int(11) NOT NULL,
  `articles_id` int(11) NOT NULL,
  `images_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `articles_menu_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_menu_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `left` varchar(255) NOT NULL,
  `right` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `left`, `right`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'akash', '', '', 0, '2016-04-27 09:43:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'maruf', '', '', 0, '2007-12-31 07:03:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'maruf1', '', '', 0, '2016-05-01 08:00:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `extention` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
`id` int(11) NOT NULL,
  `articles_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `left` varchar(255) NOT NULL,
  `right` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `personal_phone` int(11) DEFAULT NULL,
  `home_phone` int(11) DEFAULT NULL,
  `office_phone` int(11) DEFAULT NULL,
  `current_address` varchar(255) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `permanent_address`, `profile_pic`, `created_at`, `modified_at`, `deleted_at`, `modified_by`) VALUES
(21, 45, 'maruf1', 'hossain1', 1722694811, 1722694811, 1722694811, 'dhaka', 'nilphamai', '1462121528passport 5.jpg', '2016-04-27 07:08:18', '2016-05-01 06:52:08', NULL, 45),
(22, 47, 'adnan1234YHGUIYUJKds', 'siddique', 1545454556, 2147483647, 15454545, 'C/O: Md. Abu Bakar Siddique; 23/A(F# 4/B), New Paltan Line, Azimpur, Dhaka-1205.\r\n', 'C/O: Md. Rejaul Alam; Vill: Kukhapara (Dhonipara); Thana, \r\npost & District: Nilphamari-5300;  Bangladesh.\r\n', '1462120609images_039.jpg', '2016-05-01 07:15:11', '2016-05-01 06:42:44', NULL, 47),
(23, 49, 'akash', 'muhid', 1722694811, 1722694811, 1722694811, 'bangladesh', 'nilphamari', '1462119500images_006.jpg', '2016-05-01 07:49:30', '2016-05-01 06:45:28', NULL, 49),
(24, 50, 'kjgkhjg1122', 'kjhkujhg', 1516114074, 1722694811, 1722694811, 'kljlh', 'hvhvgcv', '1462121391images_015.jpg', '2007-12-31 07:31:29', '2007-12-31 07:14:18', NULL, 50);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `unique_id`, `username`, `password`, `email`, `is_admin`, `modified_at`, `created_at`, `deleted_at`, `is_active`) VALUES
(21, 0, '571744bf2f829', 'maruf', '123', 'maruf@gmail.com', 1, '2016-04-24 04:51:49', '2016-04-20 10:58:39', NULL, 1),
(45, 0, '5720f20203913', 'maruf1', 'maruf1', 'maruf1@gamil.com', 0, '2016-05-01 06:52:08', '2016-04-27 07:08:18', NULL, 1),
(47, 0, '572590dfd0b24', 'adnan', 'adnan', 'adnan@gmail.com', 0, '2016-05-01 06:42:44', '2016-05-01 07:15:11', NULL, 1),
(49, 0, '572598ea86518', 'akash1', 'akash', 'akash1@gmail.com', 0, '2016-05-01 06:45:28', '2016-05-01 07:49:30', NULL, 1),
(50, 0, '47793581ea7c8', 'rifat', 'rifat', 'rifat@gmail.com', 0, '2007-12-31 07:14:18', '2007-12-31 07:31:29', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_user_id` (`users_id`);

--
-- Indexes for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD KEY `fk_PerOrders` (`article_id`), ADD KEY `fk_article_categories` (`category_id`);

--
-- Indexes for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ar_img` (`articles_id`), ADD KEY `fk_im_ar` (`images_id`);

--
-- Indexes for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_article_menu` (`article_id`), ADD KEY `fk_menu_map` (`menu_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `password` (`password`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
ADD CONSTRAINT `fk_PerOrders` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_article_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
ADD CONSTRAINT `fk_ar_img` FOREIGN KEY (`articles_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_im_ar` FOREIGN KEY (`images_id`) REFERENCES `images` (`id`);

--
-- Constraints for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
ADD CONSTRAINT `fk_article_menu` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_menu_map` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
