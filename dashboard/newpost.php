<?php
include_once('main_nave.php');
if (!isset($_SESSION['username'])) { header('location:../login.php'); }
$categories = $obj->categories();

?>

<!--------------------------this script is for ckeditor------------------------------->
<head>
    <script src="ckeditor/ckeditor.js"></script>
</head>
<!-------------------------------------------------------------------------------->

<div id="main-body">
    <div class="row">
      
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="dashboard">
                    <div class="page-header"><h3>New Post</h3></div>
                    <?php if(isset($_SESSION['message'])){ ?>
                    <p class="alerts"><?php echo $_SESSION['message']; ?></p>
                    <?php unset($_SESSION['message']); } ?>
                    <form action="../control/newpost.php" method="POST">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="title" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subtitle" placeholder="Sub title">
                                </div>
                                
<!---------------------------------------------------------ckeditor field------------------------------------------------------------>
                                <div>
                                    <textarea class="ckeditor" id="editor" name="html_details" placeholder="Details"></textarea>
                                </div>
<!----------------------------------------------------------------------------------------------------------------------------------->
                           
                            </div>
                            <div class="col-sm-4">
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading">Select Category</div>
                                    <div class="panel-body">
                                        <?php
                                        if (!empty($categories)) {
                                            foreach ($categories as $category) {
                                        ?>
                                        <div class="checkbox"><label><input type="checkbox" value="<?php echo $category['id']; ?>" name="category[]"><?php echo $category['title']; ?></label></div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">Save post</div>
                                    <div class="panel-body">
                                        
                                        <button type="submit" class="btn btn-default btn-sm">Publish</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>