<?php
include_once('main_nave.php');
if (!isset($_SESSION['username'])) { header('location:../login.php'); }
$categories = $obj->categories();
?>
<div id="main-body">
    <div class="row">

        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>Add Category</h3></div>
                    <?php if(isset($_SESSION['message'])){ ?>
                    <p class="alerts"><?php echo $_SESSION['message']; ?></p>
                    <?php unset($_SESSION['message']); } ?>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="panel panel-default">
                                <div class="panel-heading">New category</div>
                                <div class="panel-body">
                                    <form action="../control/addcategory.php" method="POST">
                                        <div class="form-group">
                                            <label for="">Add new category</label>
                                            <input type="text" class="form-control" name="category" placeholder="New Category">
                                        </div>
                                        <button type="submit" class="btn btn-default btn-sm">Add category</button>
                                    </form>
                                </div>
                                <div class="panel-footer">New category</div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">Category List</div>
                                <div class="panel-body">
                                    <?php  if (!empty($categories)) { ?>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php foreach ($categories as $value) { ?>
                                        <tr>
                                            <td><?php echo $value['title']; ?></td>
                                            <td class="text-center">
                                                <a href="editcategory.php?id=<?php echo $value['id']; ?>"><i class="fa fa-edit">&nbsp;</i></a>
                                                <a href="../control/deletecategory.php?id=<?php echo $value['id']; ?>"><i class="fa fa-remove">&nbsp;</i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                    <?php }else{ ?>
                                    <p>No Categories</p>
                                    <?php } ?>
                                </div>
                                <div class="panel-footer">Category List</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
