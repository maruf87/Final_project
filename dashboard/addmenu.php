<?php
include_once('main_nave.php');
if (!isset($_SESSION['username'])) { header('location:../login.php'); }
$menus = $obj->menus();
$articles = $obj->articles();
?>
<div id="main-body"">
    <div class="row">
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>Add Menu</h3></div>
                    <?php if(isset($_SESSION['message'])){ ?>
                    <p class="alerts"><?php echo $_SESSION['message']; ?></p>
                    <?php unset($_SESSION['message']); } ?>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="panel panel-default">
                                <div class="panel-heading">New menu</div>
                                <div class="panel-body">
                                    <form action="../control/addmenu.php" method="POST">
                                        <div class="form-group">
                                            <label for="">Add new menu</label>
                                            <input type="text" class="form-control" name="menu" placeholder="New Menu">
                                        </div>
                                        <button type="submit" class="btn btn-default btn-sm">Add menu</button>
                                    </form>
                                </div>
                                <div class="panel-footer">New menu</div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">Menu List</div>
                                <div class="panel-body">
                                    <?php  if (!empty($menus)) { ?>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Menu</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        <?php foreach ($menus as $value) { ?>
                                        <tr>
                                            <td><?php echo $value['title']; ?></td>
                                            <td class="text-center">
                                                <a href="editmenu.php?id=<?php echo $value['id']; ?>"><i class="fa fa-edit">&nbsp;</i></a>
                                                <a href="../control/deletemenu.php?id=<?php echo $value['id']; ?>"><i class="fa fa-remove">&nbsp;</i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                    <?php }else{ ?>
                                    <p>No Menus</p>
                                    <?php } ?>
                                </div>
                                <div class="panel-footer">Menu List</div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">Add Post to menu</div>
                                <div class="panel-body">
                                    <?php  if (!empty($articles)) { ?>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Posts List</th>
                                            <th class="text-center">Add to menu</th>
                                        </tr>
                                        <?php foreach ($articles as $value) { ?>
                                        <tr>
                                            <td><?php echo $value['title']; ?></td>
                                            <td class="text-center">
                                                <a href="addmenu.php?id=<?php echo $value['id']; ?>"><i class="fa fa-plus">&nbsp;</i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                    <?php }else{ ?>
                                    <p>No Posts</p>
                                    <?php } ?>
                                </div>
                                <div class="panel-footer">Add Post to menu</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
