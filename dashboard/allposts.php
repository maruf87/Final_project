<?php
include_once 'main_nave.php';
$obj->prepare($_SESSION);
$articles = $obj->articles();
?>
<div id="main-body">
    <div class="row">
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>All posts</h3></div>
                    <?php  ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <li><a href="javaScript:;">All (<?php echo sizeof($articles)?>)</a></li>
                                <li><a href="javaScript:;">Trash (<?php echo sizeof($articles) ?>)</a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <?php if (!empty($articles)) { ?>
                            <table class="table table-responsive table-hover">
                                <tr>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Publish on</th>
                                    <th>Action</th>
                                </tr>
                                <?php foreach ($articles as $article) { ?>
                                <tr>
                                    <td>
                                    <strong><?php echo $article['title']; ?></strong>
                                    <p><?php echo $article['sub_title']; ?></p>
                                    </td>
                                    <td>
                                    <?php
                                    if ($article['is_admin'] != 0) { echo "Admin"; }else{ echo $article['username']; }
                                    ?>
                                    </td>
                                    <td>Category</td>
                                    <td>
                                    <?php
                                        $date = date_create($article['created_at']);
                                        echo date_format($date, 'd-M-Y');
                                    ?>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
<!--                                            <li><a href="editpost.php?post_id=<?php echo $article['id']; ?>" class="label label-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>-->
                                         <li><a href="#id=<?php echo $article['users_id']; ?>" class="label label-danger"><i class="fa fa-trash" aria-hidden="true"></i></li>
                                        </ul>
                                    </td>
                                </tr>
                               <?php } ?>
                            </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
