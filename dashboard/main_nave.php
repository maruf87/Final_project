<?php
if(!isset($_SESSION)){ session_start(); }
include_once('../vendor/autoload.php');
use owncms\owncms;
$obj = new owncms();
if (!isset($_SESSION['username'])) { header('location:../login.php'); }
$articles = $obj->articles();
$categories = $obj->categories();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Dashboard - CodeHunter</title>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/local.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
    <div id="wrapper">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!--div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">USER</a>
            </div-->
            <div class="navbar-header">
                    <a href="../index.php" target="_blank"><i  class="fa fa-home"></i> Home</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul id="active" class="nav navbar-nav side-nav">
                    <li class="selected"><a href="index.php"><i class="fa fa-bullseye"></i>Dashboard</a></li>
                    <li><a href="#"><i class="fa fa-tasks"></i> Post</a></li>
                    <li class="list-group-item"><a href="allposts.php">All Posts</a></li>
                    <li class="list-group-item"><a href="newpost.php">Add post</a></li>
                    <li class="list-group-item"><a href="addmenu.php">Add Manu</a></li>
                    <li class="list-group-item"><a href="addcategory.php">Add Catagory</a></li>
                    
                    <?php if($_SESSION['is_admin'] != 1){ ?>
                    <li><a href="profile.php"><i  class="fa fa-file-text-o"></i> Profile</a></li>
                    <?php } ?> 
<!--                    <li><a href="gallery.php"><i class="fa fa-list-ol"></i> Gallery</a></li>-->
              
                
                     <?php if($_SESSION['is_admin'] == 1){ ?>
                        
                     <li class="selected"><a href="allusers.php"><i class="fa fa-user">&nbsp;</i>Users</a></li>
                       
                    <?php } ?>    
                        
                     
                </ul>
            </div>
              <div>
               
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <?php if (isset($_SESSION['username'])) { ?>
                   
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Hi, <?php echo $_SESSION['username']; ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a></li>
<!--                            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>-->
                            <li class="divider"></li>
                            <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>
                     <?php } ?>
                    <li class="divider-vertical"></li>
                    <li>
                        <form class="navbar-search">
                            
                        </form>
                    </li>
                </ul>
            </div>
        </nav>

            </div>

    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: false,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            });            
        });        
    </script>
</body>
</html>