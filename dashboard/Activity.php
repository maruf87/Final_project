
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Dark Admin</title>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/local.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
</head>
<body>
    <div id="wrapper">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin Panel</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul id="active" class="nav navbar-nav side-nav">
                    <li class="selected"><a href="index.php"><i class="fa fa-bullseye"></i> Dashboard</a></li>
                    <li><a href="Add_post.html"><i class="fa fa-tasks"></i> Add Post</a></li>
                    <li><a href="Add_Page.html"><i class="fa fa-list-ol"></i> Add Page</a></li>
<!--                <li><a href="blog.html"><i class="fa fa-globe"></i> Blog</a></li>-->
                    <li><a href="Add_Menu.html"><i class="fa fa-list-ol"></i>Add Menu</a></li>
                    <li><a href="Gallery.html"><i class="fa fa-list-ol"></i> Gallery</a></li>
                   <?php if($_SESSION['is_admin'] == 1){ ?>
                     <li class="list-group-item"><a href="allusers.php"><i class="fa fa-user">&nbsp;</i>Users</a></li>
                   <?php } ?>
                    <li><a href="Advertiesment.html"><i class="fa fa-table"></i> Advertiesment</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
<!--                    <li class="dropdown messages-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages <span class="badge">2</span> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">2 New Messages</li>
                            <li class="message-preview">
                                <a href="#">
                                    <span class="avatar"><i class="fa fa-bell"></i></span>
                                    <span class="message">Security alert</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="message-preview">
                                <a href="#">
                                    <span class="avatar"><i class="fa fa-bell"></i></span>
                                    <span class="message">Security alert</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#">Go to Inbox <span class="badge">2</span></a></li>
                        </ul>
                    </li>-->
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> User <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <form class="navbar-search">
                            <input type="text" placeholder="Search" class="form-control">
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
        
        
<!-------------------------------------------- ADD post Here----------------------------------->
        
            
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>All Users</h3></div>
                    <?php if(isset($_SESSION['message'])){ ?>
                    <p class="alerts"><?php echo $_SESSION['message']; ?></p>
                    <?php unset($_SESSION['message']); } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <?php if (!empty($allusers)) { ?>
                                <li><a href="allusers.php">All (<?php echo sizeof($allusers) ?>)</a></li>
                                <?php } ?>
                                <?php if (!empty($trashuser)) { ?>
                                <li><a href="allusers.php?users=trash">Trash (<?php echo sizeof($trashuser) ?>)</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <table class="table table-responsive table-hover">
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Posts</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                <?php
                                if (empty($_GET)) {
                                    if (!empty($allusers)) {
                                        foreach ($allusers as $user) {
                                ?>
                                <tr>
                                    <td>                                    
                                    <span class="img">
                                        <?php if(!empty($user['profile_pic'])){ ?>
                                        <img src="images/<?php echo $user['profile_pic']; ?>" alt="" class="img-responsive">
                                        <?php }else{ ?>
                                        <i class="fa fa-user fa-4x" aria-hidden="true"></i>
                                        <?php } ?>
                                    </span>
                                    <span><?php echo $user['username']; ?></span>
                                    </td>
                                    <td>
                                        <?php
                                        echo $user['first_name']." ";
                                        echo $user['last_name'];
                                        ?>
                                    </td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php if ($user['is_admin'] == 1) { echo "Admin"; }else{ echo "User"; } ?></td>
                                    <td>6</td>
                                    <td class="text-center">
                                        <?php if ($user['is_admin'] != 1){ ?>
                                        <ul class="list-inline">
                                            <?php if ($user['is_active'] == NULL){ ?>
                                            <li><a href="control/action.php?confirm=<?php echo $user['unique_id']; ?>" class="label label-success">Confirm</a></li>
                                            <li><a href="control/action.php?delete=<?php echo $user['unique_id']; ?>" class="label label-danger">Delete</a></li>
                                            <?php }else{ ?>
                                            <?php if ($user['is_active'] == 1){ ?>
                                            <li><a href="control/action.php?disable=<?php echo $user['unique_id']; ?>" class="label label-warning">Blocked</a></li>
                                            <?php }elseif($user['is_active'] == 2){ ?>
                                            <li><a href="control/action.php?active=<?php echo $user['unique_id']; ?>" class="label label-primary">Unblocked</a></li>
                                            <?php } ?>
                                            <li><a href="control/action.php?trash=<?php echo $user['unique_id']; ?>" class="label label-danger">Trash</a></li>
                                            <?php } ?>
                                        </ul>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                        }
                                    }else{
                                ?>
                                <tr><td colspan="6">No Users</td></tr>
                                <?php
                                    }
                                }elseif ($_GET['users'] == 'trash') {
                                    if (!empty($trashuser)) {
                                        foreach ($trashuser as $user) {
                                ?>
                                <tr>
                                    <td>
                                    <?php if(!empty($user['profile_pic'])){ ?>
                                    <span class="img"><img src="images/<?php echo $user['profile_pic']; ?>" alt="" class="img-responsive"></span>
                                    <?php } ?>
                                    <span><?php echo $user['username']; ?></span>
                                    </td>
                                    <td>
                                        <?php
                                        echo $user['first_name']." ";
                                        echo $user['last_name'];
                                        ?>
                                    </td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php if ($user['is_admin'] == 1) { echo "Admin"; }else{ echo "User"; } ?></td>
                                    <td>6</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li><a href="control/action.php?restore=<?php echo $user['unique_id']; ?>" class="label label-success">Restore</a></li>
                                            <li><a href="control/action.php?delete=<?php echo $user['unique_id']; ?>" class="label label-danger">Delete</a></li>
                                        </ul>
                                    </td>
                                </tr>
                                <?php
                                        }
                                    }else{
                                ?>
                                <tr><td colspan="6">No Users</td></tr>
                                <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <ul class="list-inline">
                                <?php if (!empty($allusers)) { ?>
                                <li><a href="allusers.php">All (<?php echo sizeof($allusers) ?>)</a></li>
                                <?php } ?>
                                <?php if (!empty($trashuser)) { ?>
                                <li><a href="allusers.php?users=trash">Trash (<?php echo sizeof($trashuser) ?>)</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

            
       
        
        
        
        
    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        jQuery(function ($) {
            var performance = [12, 43, 34, 22, 12, 33, 4, 17, 22, 34, 54, 67],
                visits = [123, 323, 443, 32],
                traffic = [
                {
                    Source: "Direct", Amount: 323, Change: 53, Percent: 23, Target: 600
                },
                {
                    Source: "Refer", Amount: 345, Change: 34, Percent: 45, Target: 567
                },
                {
                    Source: "Social", Amount: 567, Change: 67, Percent: 23, Target: 456
                },
                {
                    Source: "Search", Amount: 234, Change: 23, Percent: 56, Target: 890
                },
                {
                    Source: "Internal", Amount: 111, Change: 78, Percent: 12, Target: 345
                }];


            $("#shieldui-chart1").shieldChart({
                theme: "dark",

                primaryHeader: {
                    text: "Visitors"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "area",
                    collectionAlias: "Q Data",
                    data: performance
                }]
            });

            $("#shieldui-chart2").shieldChart({
                theme: "dark",
                primaryHeader: {
                    text: "Traffic Per week"
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                dataSeries: [{
                    seriesType: "pie",
                    collectionAlias: "traffic",
                    data: visits
                }]
            });

            $("#shieldui-grid1").shieldGrid({
                dataSource: {
                    data: traffic
                },
                sorting: {
                    multiple: true
                },
                rowHover: false,
                paging: false,
                columns: [
                { field: "Source", width: "170px", title: "Source" },
                { field: "Amount", title: "Amount" },                
                { field: "Percent", title: "Percent", format: "{0} %" },
                { field: "Target", title: "Target" },
                ]
            });            
        });        
    </script>
</body>
</html>
