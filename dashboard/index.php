<?php include_once 'main_nave.php';
$obj->prepare($_SESSION);
$articles = $obj->articles();
?>

<html>
    <body>
        <div id="main-body">
            <h1>Welcome to our CMS site.</h1><br><br>
        <div class="row">
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>All posts</h3></div>
                    <?php  ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <li><a href="javaScript:;">All (<?php echo sizeof($articles)?>)</a></li>
<!--                                <li><a href="javaScript:;">Trash (<?php echo sizeof($articles) ?>)</a></li>-->
                            </ul>
                        </div>
                        <div class="panel-body">
                            <?php if (!empty($articles)) { ?>
                            <table class="table table-responsive table-hover">
                                <tr>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Details</th>
                                    <th>Publish on</th>                                  
                                </tr>
                                <?php foreach ($articles as $article) { ?>
                                <tr>
                                    <td>
                                    <strong><?php echo $article['title']; ?></strong>
                                    </td>
                                    <td>
                                    <p><?php echo $article['sub_title']; ?></p>
                                    </td>
                                    <td>
                                     <p><?php echo $article['details']; ?></p>
                                    </td>
                                    <td>
                                    <?php
                                        $date = date_create($article['created_at']);
                                        echo date_format($date, 'd-M-Y');
                                    ?>
                                    </td>                                   
                                </tr>
                                <?php } ?>
                            </table>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
<!--                            <ul class="list-inline">
                                <li><a href="javaScript:;">All (5)</a></li>
                                <li><a href="javaScript:;">Trash (5)</a></li>
                            </ul>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        
</body>
</html>