<?php
include_once('../vendor/autoload.php');
use owncms\owncms;

$obj = new owncms();
$_POST['user_id'] = $_SESSION['user_id'];
$_POST['details'] = strip_tags($_POST['html_details']);
$array = $_POST['category'];
$imploded = implode(",",$array);
$_POST['category'] = $imploded;
$obj->prepare($_POST);
$obj->newpost();
?>